#include <cstdlib>
#include <cfloat>
#include <chrono>
#include <iostream>

using namespace std;

#define DEF_N 24

typedef std::chrono::high_resolution_clock Time;
typedef std::chrono::duration<float> fsec;


void dataInitialization(double *x, int n){
    for (int i=0; i < n; ++i)
        x[i] = i;
}

double dotVecs(double *a, double *b, int n){
    double total_dot = 0.0;

    /* Вычисление локальных максимумов */
    for ( int i = 0; i < n; i++ )
        total_dot += a[i] * b[i];
    
    return total_dot;
}

int main(int argc, char* argv[]) {
    /* Внешние данные */
    int n = DEF_N;
    char * ENV_N = getenv("N");
    if (ENV_N)
        n = atoi(ENV_N);

    /* Объявление данных */
    double *a = new double[n];
    double *b = new double[n];
    
    /* Подготовка данных */
    dataInitialization(a, n);
    dataInitialization(b, n);
    
    /* Вычисление */
    auto time_start = Time::now();
    double result = dotVecs(a, b, n);
    auto time_end = Time::now();
    fsec fs = time_end - time_start;

    /* Вывод результата */
    // printf("Total dot = %10.2f\n", result);
    // printf("Time = %18.6f\n", fs.count());
    printf("%0.6f\n", fs.count());

    return 0;
}
