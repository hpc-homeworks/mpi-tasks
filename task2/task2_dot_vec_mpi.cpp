#include <mpi.h>
#include <cstdlib>
#include <cfloat>
#include <iostream>

using namespace std;

#define DEF_N 24

void dataInitialization(double *x, int n){
    for (int i=0; i < n; ++i)
        x[i] = i;
}

double dotVecs(double *a, double *b, int n, int proc_rank, int proc_num){
    double total_dot, proc_dot = 0.0;
    int proc_n = n / proc_num;

    /* Рассылка данных */
    MPI_Bcast(a, n, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    MPI_Bcast(b, n, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    /* Вычисление локальных максимумов */
    int i1 = proc_n * proc_rank;
    int i2 = proc_n * ( proc_rank + 1 );
    if ( proc_rank == proc_num - 1 ) i2 = n;
    for ( int i = i1; i < i2; i++ )
        proc_dot += a[i] * b[i];

    /* Редукция максимумов */
    MPI_Reduce(&proc_dot, &total_dot, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
    
    return total_dot;
}

int main(int argc, char* argv[]) {
    /* Внешние данные */
    int n = DEF_N;
    char * ENV_N = getenv("N");
    if (ENV_N)
        n = atoi(ENV_N);

    /* Объявление данных */
    int proc_num, proc_rank;
    double *a = new double[n];
    double *b = new double[n];

    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &proc_num);
    MPI_Comm_rank(MPI_COMM_WORLD, &proc_rank);
    
    /* Подготовка данных */
    if ( proc_rank == 0 ){
        dataInitialization(a, n);
        dataInitialization(b, n);
    }
    
    /* Вычисление */
    double time_start = MPI_Wtime();
    double result = dotVecs(a, b, n, proc_rank, proc_num);
    double time_end = MPI_Wtime();

    /* Вывод результата */
    if ( proc_rank == 0 ){
        // printf("Total dot = %10.2f\n", result);
        // printf("Time = %18.6f\n", time_end - time_start);
        printf("%0.6f\n", time_end - time_start);
    }

    MPI_Finalize();

    return 0;
}
