#include <cstdlib>
#include <cmath>
#include <cfloat>
#include <chrono>
#include <iostream>

using namespace std;

#define DEF_N 100

typedef std::chrono::high_resolution_clock Time;
typedef std::chrono::duration<float> fsec;

double f(double x) {
    return std::log2(x);
} 

double count_integral(double a, double b, int n){
    /* Настройка алгоритма */
    double h = (b - a) / n;
    double total_sum = 0.0;
    double x;

    for ( int i = 0; i < n; i++ ) {
        x = a + h * i;
        total_sum += f(x);
    }

    return total_sum;
}


int main(int argc, char* argv[]) {
    int n = DEF_N;
    char * ENV_N = getenv("N");
    if (ENV_N)
        n = atoi(ENV_N);

    /* Инициализация данных */
    double a = 1, b = 5;

    /* Вычисление */
    auto time_start = Time::now();
    double result = count_integral(a, b, n);
    auto time_end = Time::now();
    fsec fs = time_end - time_start;

    /* Вывод результата */
    // printf("Total sum = %10.2f\n", result);
    // printf("Time = %18.6f\n", fs.count());
    printf("%0.6f\n", fs.count());

}
