#include <mpi.h>
#include <cmath>
#include <cstdlib>
#include <cfloat>
#include <iostream>

using namespace std;

#define DEF_N 100

double f(double x) {
    return std::log2(x);
} 

double count_integral(double a, double b, int n, int proc_rank, int proc_num){
    /* Настройка алгоритма */
    double h = (b - a) / n;
    double proc_sum = 0.0;
    double total_sum = 0.0;
    double x;
    int proc_n = n / proc_num;

    int i1 = proc_n * proc_rank;
    int i2 = proc_n * ( proc_rank + 1 );
    if ( proc_rank == proc_num - 1 ) i2 = n;
    for ( int i = i1; i < i2; i++ ) {
        x = a + h * i;
        proc_sum += f(x);
    }

    /* Редукция сумм */
    MPI_Reduce(&proc_sum, &total_sum, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);

    return total_sum;
}


int main(int argc, char* argv[]) {
    int n = DEF_N;
    char * ENV_N = getenv("N");
    if (ENV_N)
        n = atoi(ENV_N);

    /* Инициализация данных */
    int proc_num, proc_rank;
    double a = 1, b = 5;

    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &proc_num);
    MPI_Comm_rank(MPI_COMM_WORLD, &proc_rank);

    /* Вычисление */
    double time_start = MPI_Wtime();
    double result = count_integral(a, b, n, proc_rank, proc_num);
    double time_end = MPI_Wtime();

    /* Вывод результата */
    if ( proc_rank == 0 ){
        // printf("Total sum = %10.2f\n", result);
        // printf("Time = %18.6f\n", time_end - time_start);
        printf("%0.6f\n", time_end - time_start);
    }

    MPI_Finalize();
}
