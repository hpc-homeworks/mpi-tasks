#include <mpi.h>
#include <cstdlib>
#include <cfloat>
#include <iostream>

using namespace std;

#define DEF_N 100

void dataInitialization(double **a, int n){
    for (int i=0; i < n; ++i)
        for (int j=0; j < n; ++j)
            a[i][j] = i;
}

double findMaxMin(double **a, int n, int proc_rank, int proc_num){
    double total_max = -DBL_MAX;
    double total_min;
    double proc_min;
    int proc_n = n / proc_num;

    for(int i=0; i<n; i++){
        MPI_Bcast(a[i], n, MPI_DOUBLE, 0, MPI_COMM_WORLD);

        /* Вычисление локальных минимумов */
        int j1 = proc_n * proc_rank;
        int j2 = proc_n * ( proc_rank + 1 );
        if ( proc_rank == proc_num - 1 ) j2 = n;
        total_min = DBL_MAX;
        proc_min = DBL_MAX;
        for ( int j = j1; j < j2; j++ )
            if (a[i][j] < proc_min)
                proc_min = a[i][j];

        /* Редукция минимумов */
        MPI_Reduce(&proc_min, &total_min, 1, MPI_DOUBLE, MPI_MIN, 0, MPI_COMM_WORLD);

        if ( proc_rank == 0 )
            if (total_min > total_max)
                total_max = total_min;
        MPI_Barrier(MPI_COMM_WORLD);
    }

    return total_max;
}

int main(int argc, char* argv[]) {
    /* Внешние данные */
    int n = DEF_N;
    char * ENV_N = getenv("N");
    if (ENV_N)
        n = atoi(ENV_N);

    /* Объявление данных */
    int proc_num, proc_rank;
    double **a = new double*[n];
    for (int i=0; i < n; ++i)
        a[i] = new double[n];

    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &proc_num);
    MPI_Comm_rank(MPI_COMM_WORLD, &proc_rank);
    
    /* Подготовка данных */
    if ( proc_rank == 0 ) dataInitialization(a, n);
    MPI_Barrier(MPI_COMM_WORLD);
    /* Вычисление */
    double time_start = MPI_Wtime();
    double result = findMaxMin(a, n, proc_rank, proc_num);
    double time_end = MPI_Wtime();

    /* Вывод результата */
    if ( proc_rank == 0 ){
        // printf("Total max = %10.2f\n", result);
        // printf("Time = %18.6f\n", time_end - time_start);
        printf("%0.6f\n", time_end - time_start);
    }

    MPI_Finalize();

    return 0;
}
