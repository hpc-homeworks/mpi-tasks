#include <cstdlib>
#include <cfloat>
#include <chrono>
#include <iostream>

using namespace std;

#define DEF_N 100

typedef std::chrono::high_resolution_clock Time;
typedef std::chrono::duration<float> fsec;

void dataInitialization(double **a, int n){
    for (int i=0; i < n; ++i)
        for (int j=0; j < n; ++j)
            a[i][j] = i;
}

double findMaxMin(double **a, int n){
    double total_max = -DBL_MAX;
    double total_min;

    for(int i=0; i<n; i++){
        total_min = DBL_MAX;
        for ( int j = 0; j < n; j++ )
            if (a[i][j] < total_min)
                total_min = a[i][j];

        if (total_min > total_max)
            total_max = total_min;
    }

    return total_max;
}

int main(int argc, char* argv[]) {
    /* Внешние данные */
    int n = DEF_N;
    char * ENV_N = getenv("N");
    if (ENV_N)
        n = atoi(ENV_N);

    /* Объявление данных */
    int proc_num, proc_rank;
    double **a = new double*[n];
    for (int i=0; i < n; ++i)
        a[i] = new double[n];
    
    /* Подготовка данных */
    dataInitialization(a, n);
    /* Вычисление */
    auto time_start = Time::now();
    double result = findMaxMin(a, n);
    auto time_end = Time::now();
    fsec fs = time_end - time_start;

    /* Вывод результата */
    // printf("Total max = %10.2f\n", result);
    printf("%0.6f\n", fs.count());

    return 0;
}
