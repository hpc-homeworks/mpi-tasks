import os
import pandas as pd
import matplotlib.pyplot as plt
from dotenv import load_dotenv


def plot_parallel():
    df = pd.read_csv(os.environ.get('out_parallel_csv_file'))
    df.plot(
        figsize=(10, 5),
        x='nprocs', y='ex_time',
        xlabel='Кол-во процессов', ylabel='Время работы (сек)')
    plt.savefig(os.environ.get('out_parallel_png_file'))
    plt.show()
    
def plot_n():
    df = pd.read_csv(os.environ.get('out_n_csv_file'))
    df['mode'] = df['with_mpi'].apply(lambda x: 'with_mpi' if x == 1 else 'without_mpi')
    df = df.pivot(index='n', columns='mode', values='ex_time')
    df.plot(
        figsize=(10, 5),
        xlabel='Размер задачи (N)', ylabel='Время работы (сек)')
    plt.savefig(os.environ.get('out_n_png_file'))
    plt.show()


def plot_S():
    df = pd.read_csv(os.environ.get('out_n_csv_file'))
    df_omp = df[df['with_mpi'] == 1].reset_index()
    df = df[df['with_mpi'] == 0].reset_index()
    S = df['ex_time'] / df_omp['ex_time']
    df_S = pd.DataFrame({
        'n': df['n'],
        'S': S
    }) 
    df_S.plot(figsize=(10, 5), x='n', y='S')
    df_S.to_csv(os.environ.get('out_S_csv_file'))
    plt.savefig(os.environ.get('out_S_png_file'))
    plt.show()
    

if __name__ == '__main__':
    # load_dotenv('./task1/.env')
    # load_dotenv('./task2/.env')
    # load_dotenv('./task4/.env')
    load_dotenv('./task5/.env')
    
    plot_parallel()
    plot_n()
    plot_S()
