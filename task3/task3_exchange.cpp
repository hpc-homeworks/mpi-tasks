#include <mpi.h>
#include <iostream>

using namespace std;

#define DEF_N 10
#define DEF_M 10

double loopProcess0(int m, int n){
    int dest = 1;
    double time_start, time_end, total_time = 0.0;

    char* send_msg = new char[m];
    for (int i = 0; i < n; ++i)
        send_msg[i] = 'c';

    for (int i = 0; i < n; ++i){
        printf("Send %i message: START\n", i);
        time_start = MPI_Wtime();
        MPI_Rsend(&send_msg, m, MPI_CHAR, dest, i, MPI_COMM_WORLD);
        time_end = MPI_Wtime();
        printf("Send %i message: END\n", i);
        total_time +=  (time_end - time_start) / n;
    }
    return total_time;  
}

void loopProcess1(int m, int n){
    int source = 0;
    MPI_Status status;
    char* recv_msg = new char[m];
    for (int j = 0; j < n; ++j){
        printf("Recv %i message: START\n", j);
        MPI_Recv(&recv_msg, m, MPI_CHAR, source, j, MPI_COMM_WORLD, &status);
        printf("Recv %i message: END\n", j);
        cout << recv_msg[0] << endl;
    }
}

int main(int argc, char* argv[]) {
    /* Внешние данные */
    int n = DEF_N;
    char * ENV_N = getenv("N");
    if (ENV_N)
        n = atoi(ENV_N);
    
    int m = DEF_N;
    char * ENV_M = getenv("M");
    if (ENV_M)
        m = atoi(ENV_M);

    /* Объявление данных */
    int ProcNum, ProcRank, RecvRank;

    double time;
    

    MPI_Init ( &argc, &argv );
    MPI_Comm_size ( MPI_COMM_WORLD, &ProcNum);
    MPI_Comm_rank ( MPI_COMM_WORLD, &ProcRank);

    if ( ProcRank == 0 )
    {
        time = loopProcess0(m, n);
        printf("%0.10f\n", time);
        fflush(stdout);
    }
    else
        loopProcess1(m, n);

    MPI_Finalize();

    return 0;
}
