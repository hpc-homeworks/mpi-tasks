#include <mpi.h>
#include <iostream>

using namespace std;

#define DEF_N 10
#define DEF_M 10


int main(int argc, char* argv[]) {
    /* Внешние данные */
    int n = DEF_N;
    char * ENV_N = getenv("N");
    if (ENV_N)
        n = atoi(ENV_N);
    
    int m = DEF_N;
    char * ENV_M = getenv("M");
    if (ENV_M)
        m = atoi(ENV_M);

    

    MPI_Init ( &argc, &argv );

    /* Объявление данных */
    int ProcNum, ProcRank, RecvRank;
    double time_start, time_end, total_time = 0.0;
    int i = 0;
    char** send_msgs = new char*[n];
    char** recv_msgs = new char*[n];

    MPI_Comm_size ( MPI_COMM_WORLD, &ProcNum);
    MPI_Comm_rank ( MPI_COMM_WORLD, &ProcRank);

    MPI_Status status;

    if ( ProcRank == 0 ){
        for (int i = 0; i < n; ++i)
            send_msgs[i] = new char[m];
            for (int j = 0; j < m; ++j)
                send_msgs[i][j] = 'c';
    }
    int c;

    while (i < n){
        printf("[%i] %i message: START\n", ProcRank, i);
        send_msgs[i] = new char[m];
        recv_msgs[i] = new char[m];
        if ( ProcRank == 0 ){
            for (int j = 0; j < m; ++j)
                send_msgs[i][j] = 'c';
            time_start = MPI_Wtime();
        }
        MPI_Sendrecv(
            &send_msgs[i], m, MPI_CHAR, 1, i, 
            &recv_msgs[i], m, MPI_CHAR, 0, i, 
            MPI_COMM_WORLD, &status);
        if ( ProcRank == 0 ){
            total_time +=  (time_end - time_start) / n;
            printf("[%i] %i message: END\n", ProcRank, i);
            i++;
        }
        else{
            MPI_Get_count(&status, MPI_CHAR, &c);
            printf("[%i] %i message (%i): END\n", ProcRank, i, c);
            for (int j = 0; j < m; ++j)
                cout << recv_msgs[i][j];
            cout << endl;
        }
        MPI_Barrier(MPI_COMM_WORLD);
        // printf("[%i] %i message: END\n", ProcRank, i);
    }

    /* Вывод результата */
    if ( ProcRank == 0 )
        printf("%0.6f\n", total_time);

    MPI_Finalize();

    return 0;
}
