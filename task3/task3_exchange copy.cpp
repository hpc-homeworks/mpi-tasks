#include<mpi.h>
#include<iostream>

using namespace std;

#define DEF_N 100
#define DEF_M 100

double loopProcess0(int m, int n){
    int dest = 1;
    double time_start, time_end, total_time = 0.0;
    char* send_msg;
    for (int i = 0; i < n; ++i){
        // printf("Send %i message\n", i);
        send_msg = new char[m];
        time_start = MPI_Wtime();
        MPI_Send(&send_msg, m, MPI_CHAR, dest, 0, MPI_COMM_WORLD);
        time_end = MPI_Wtime();
        total_time +=  (time_end - time_start) / n;
    }
    return total_time;  
}

void loopProcess1(int m, int n){
    int source = 0;
    MPI_Status status;
    char** recv_msgs = new char*[n];
    for (int i = 0; i < n; ++i){
        // printf("Recv %i message\n", i);
        MPI_Recv(&recv_msgs[i], m, MPI_CHAR, source, 0, MPI_COMM_WORLD, &status);
    }
}

int main(int argc, char* argv[]) {
    /* Внешние данные */
    int n = DEF_N;
    char * ENV_N = getenv("N");
    if (ENV_N)
        n = atoi(ENV_N);
    
    int m = DEF_N;
    char * ENV_M = getenv("M");
    if (ENV_M)
        m = atoi(ENV_M);

    /* Объявление данных */
    int ProcNum, ProcRank, RecvRank;
    char *send_msg = new char[m], recv_msg;

    double time;
    

    MPI_Init ( &argc, &argv );
    MPI_Comm_size ( MPI_COMM_WORLD, &ProcNum);
    MPI_Comm_rank ( MPI_COMM_WORLD, &ProcRank);

    if ( ProcRank == 0 )
    {
        time = loopProcess0(m, n);
        printf("%0.6f\n", time);
    }

    else
        loopProcess1(m, n);

    MPI_Finalize();

    return 0;
}
