
### Процессы
- `int MPI_Comm_size ( MPI_Comm comm, int *size )` - кол-во процессов
- `int MPI_Comm_rank ( MPI_Comm comm, int *rank )` - ранг просесса

### Передача/приём сообщений (Point-to-Point: Standard)
- `int MPI_Send(void *buf, int count, MPI_Datatype type, int dest, int tag, MPI_Comm comm)`
    - buf – буффер для отправляемого сообщения
    - count – размер сообщения
    - type - тип данных сообщения
    - dest - ранг получателя
    - tag - id сообщения
    - comm - коммуникатор
> Блокирующая
- `int MPI_Recv(void *buf, int count, MPI_Datatype type, int source, int tag, MPI_Comm comm, MPI_Status *status)`
    - buf, count, type – аналогично
    - source - ранг отправителя (или MPI_ANY_SOURCE)
    - tag - id сообщения (или MPI_ANY_TAG)
    - comm - коммуникатор
    - status – мета информация результата пересылки
        - status.MPI_SOURCE – ранг отправителя
        - status.MPI_TAG - id сообщения
> Блокирующая
- `MPI_Get_count(MPI_Status *status, MPI_Datatype type, int *count)` - размер принятого сообщения

### Коды ошибок
- MPI_ERR_BUFFER – неправильный указатель на буфер,
- MPI_ERR_COMM – неправильный коммуникатор,
- MPI_ERR_RANK – неправильный ранг процесса 

### Время
- `double MPI_Wtime(void)` - текущее время в сек
- `double MPI_Wtick(void)` - точность

### Коллективные операции
- 1-->все (_широковещательная рассылка_): `int MPI_Bcast(void *buf,int count,MPI_Datatype type,int root,MPI_Comm comm)`
    - buf, count, type – для приёма и передачи
    - root - ранг отправителя (рассылаемого)
    - comm - коммуникатор
- все-->1 (+ операция) (_редукция данных_): `int MPI_Reduce(void *sendbuf, void *recvbuf,int count,MPI_Datatype type, MPI_Op op,int root,MPI_Comm comm)`
    - sendbuf - буффер отправителя
    - recvbuf – буффер получателя (результирующий)
    - count - размер сообщений
    - type – тип данных сообщений
    - op - операция над данными (MPI_SUM, MPI_MAX, MPI_MIN, ...)
    - root - ранг процесса с результатом
    - comm - куммуникатор

### Синхронизация
- `int MPI_Barrier(MPI_Comm comm)`

### Передача сообщений (Point-to-Point)
- `MPI_Send` - стандартное
- `MPI_Bsend` - буферизованное (по завершению коппирования в системный буфер)
> Большие расходы памяти. Для коротких сообщений.
- `MPI_Ssend` - синхронное (по получению)
> Медленный, но надёжный. Для больших сообщений.
- `MPI_Rsend` - по готовности (по завершению приёма)
> Самый быстрый, но треб.гарантия готовности приёма

Для буф.режима:
- `int MPI_Buffer_attach(void *buf, int size)` - подключение буфера
-  `int MPI_Buffer_detach(void *buf, int *size)`- отключение буфера

Для MPI_Recv таких режимов нет.

### Передача/приём сообщений (Point-to-Point: не блокирующая)
- int `MPI_Isend` (void *buf, int count, MPI_Datatype type, int dest, int tag, MPI_Comm comm, MPI_Request *request)
- int `MPI_Issend` (void *buf, int count, MPI_Datatype type, int dest, int tag, MPI_Comm comm, MPI_Request *request)
- int `MPI_Ibsend` (void *buf, int count, MPI_Datatype type, int dest, int tag, MPI_Comm comm, MPI_Request *request)
- int `MPI_Irsend` (void *buf, int count, MPI_Datatype type, int dest,int tag, MPI_Comm comm, MPI_Request *request)
- int `MPI_Irecv` (void *buf, int count, MPI_Datatype type, int source, int tag, MPI_Comm comm, MPI_Request *request)

Добавляет `request` для проверки завершения.
- `int MPI_Test( MPI_Request *request, int *flag, MPI_status *status)`
    - request - дескриптор операции, определенный при вызове неблок.функции
    - flag - результат проверки (true, если завершена)
    - status - результат обмена (при завершении)
> Неблокирующая
- `int MPI_Wait( MPI_Request *request, MPI_status *status)` - MPI_Test с while

Доп.:
- `MPI_Testall` - проверка завершения ВСЕХ операций обмена
- `MPI_Waitall` – ожидание завершения ВСЕХ операций обмена
- `MPI_Testany` - проверка завершения ХОТЯ БЫ ОДНОЙ операции обмена
- `MPI_Waitany` – ожидание завершения ХОТЯ БЫ ОДНОЙ операции обмена
- `MPI_Testsome` - проверка завершения ЗАДАННЫХ операций обмена
- `MPI_Waitsome` - ожидание завершения ЗАДАННЫХ операций обмена

### Передача/приём сообщений (Point-to-Point: одновременный)
- `int MPI_Sendrecv(void *sbuf,int scount,MPI_Datatype stype,int dest, int stag, void *rbuf,int rcount,MPI_Datatype rtype,int source,int rtag, MPI_Comm comm, MPI_Status *status)`,
    - sbuf, scount, stype, dest, stag - параметры передаваемого сообщения
    - rbuf, rcount, rtype, source, rtag - параметры принимаемого сообщения
    - comm - коммуникатор
    - status – статус обмена
- `int MPI_Sendrecv_replace (void *buf, int count, MPI_Datatype type, int dest, int stag, int source, int rtag, MPI_Comm comm, MPI_Status* status)` - с одним буфером

### Коллективные операции (обобщённые)
- 1-->все (_распределение данных_) `int MPI_Scatter(void *sbuf,int scount,MPI_Datatype stype, void *rbuf,int rcount,MPI_Datatype rtype, int root, MPI_Comm comm)`
    - sbuf, scount, stype - передаваемое. scount - на каждый процесс
    - rbuf, rcount, rtype - принимаемое
    - root - ранг отправителя
    - comm - коммуникатор
> С разным размером: MPI_Scatterv
- все-->1 (_сбор данных_) `int MPI_Gather(void *sbuf,int scount,MPI_Datatype stype, void *rbuf,int rcount,MPI_Datatype rtype, int root, MPI_Comm comm)`
    - sbuf, scount, stype - передаваемое
    - rbuf, rcount, rtype - принимаемое
    - root – ранг получателя
    - comm - коммуникатор
- все-->все (_сбор данных_) `int MPI_Allgather(void *sbuf, int scount, MPI_Datatype stype, void *rbuf, int rcount, MPI_Datatype rtype, MPI_Comm comm)`
> С разным размером: MPI_Gatherv и MPI_Allgatherv
- все-->все (_передача данных_) `int MPI_Alltoall(void *sbuf,int scount,MPI_Datatype stype, void *rbuf,int rcount,MPI_Datatype rtype,MPI_Comm comm)`
    - sbuf, scount, stype - передаваемое. scount - на каждый процесс
    - rbuf, rcount, rtype - принимаемое
    - comm - коммуникатор
> С разным размером: MPI_Alltoallv
- все-->все (_редукция данных_):
    - `int MPI_Allreduce(void *sendbuf, void *recvbuf,int count,MPI_Datatype type, MPI_Op op,MPI_Comm comm)`
    - `MPI_Reduce_scatter` - рассылка
- все-->все (_частичная редукция данных_): `int MPI_Scan(void *sendbuf, void *recvbuf,int count,MPI_Datatype type, MPI_Op op,MPI_Comm comm)`
