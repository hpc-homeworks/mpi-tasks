#include <mpi.h>
#include <cstdlib>
#include <iostream>

using namespace std;

void DataInitialization(double *x, int N){
    for (int i=0; i < N; ++i)
        x[i] = rand() % 100;
}

int main(int argc, char* argv[]) {
    /* Объявление данных */
    int ProcNum, ProcRank, N=100;
    double *x = new double[N];
    double TotalSum, ProcSum = 0.0;
    MPI_Status Status;

    MPI_Init ( &argc, &argv );
    MPI_Comm_size(MPI_COMM_WORLD,&ProcNum);
    MPI_Comm_rank(MPI_COMM_WORLD,&ProcRank);
    
    /* Подготовка данных */
    if ( ProcRank == 0 ) DataInitialization(x,N);
    
    /* Рассылка данных */
    MPI_Bcast(x, N, MPI_DOUBLE, 0, MPI_COMM_WORLD);

    /* Вычисление частичной суммы */
    int k = N / ProcNum;
    int i1 = k * ProcRank;
    int i2 = k * ( ProcRank + 1 );
    if ( ProcRank == ProcNum-1 ) i2 = N;
    for ( int i = i1; i < i2; i++ )
        ProcSum = ProcSum + x[i];
    
    /* Сборка частичных сумм */
    // if ( ProcRank == 0 ) {
    //     TotalSum = ProcSum;
    //     for ( int i=1; i < ProcNum; i++ ) {
    //         MPI_Recv(&ProcSum, 1, MPI_DOUBLE, MPI_ANY_SOURCE, 0, MPI_COMM_WORLD, &Status);
    //         TotalSum = TotalSum + ProcSum;
    //     }
    // }
    // else
    //     MPI_Send(&ProcSum, 1, MPI_DOUBLE, 0, 0, MPI_COMM_WORLD);
    MPI_Reduce(&ProcSum,&TotalSum, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);

    /* Вывод результата */
    if ( ProcRank == 0 )
        printf("Total Sum = %10.2f\n",TotalSum); 

    MPI_Finalize();

    return 0;
}
