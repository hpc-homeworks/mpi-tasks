#!/bin/sh
set -ex

module load gcc/9
module load openmpi

max_procs=64
step_procs=2
min_procs=2
num_procs=8
max_n=100000
step_n=10000
min_n=1000

bench_parallel(){
    # Set up out file
    if [[ -f "$out_parallel_csv_file" ]]
    then
        rm $out_parallel_csv_file  
    fi
    printf '%s\n' "nprocs,ex_time" >> $out_parallel_csv_file

    # Build
    mpic++ $mpi_path_to_code -o $mpi_path_to_exe

    # Run
    export N=$min_n
    for nprocs in $(seq $min_procs $step_procs $max_procs)
    do
        ex_time=$( mpirun -np $nprocs $mpi_path_to_exe )
        printf "%s\n" "$nprocs,$ex_time" >> $out_parallel_csv_file
    done

    rm $mpi_path_to_exe
}

bench_n(){
    # Set up out file
    if [[ -f "$out_n_csv_file" ]]
    then
        rm $out_n_csv_file
    fi
    printf '%s\n' "n,with_mpi,ex_time" >> $out_n_csv_file

    # Build
    g++ $path_to_code -o $path_to_exe

    # Run
    for n in $(seq $min_n $step_n $max_n)
    do
        export N=$n
        ex_time=$( $path_to_exe )
        printf "%s\n" "$n,0,$ex_time" >> $out_n_csv_file
    done

    # Build
    mpic++ $mpi_path_to_code -o $mpi_path_to_exe

    # Run
    for n in $(seq $min_n $step_n $max_n)
    do
        export N=$n
        ex_time=$( mpirun -np $num_procs $mpi_path_to_exe )
        printf "%s\n" "$n,1,$ex_time" >> $out_n_csv_file
    done

    rm $mpi_path_to_exe
}

# source ./task1/.env
# source ./task2/.env
# source ./task4/.env
source ./task5/.env

# bench_parallel
bench_n
