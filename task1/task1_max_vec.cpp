#include <cstdlib>
#include <cfloat>
#include <chrono>
#include <iostream>

using namespace std;

#define DEF_N 24

typedef std::chrono::high_resolution_clock Time;
typedef std::chrono::duration<float> fsec;


void dataInitialization(double *x, int n){
    for (int i=0; i < n; ++i)
        x[i] = i;
}

double findMaxElement(double *x, int n){
    double total_max = -DBL_MAX;

    /* Вычисление локальных максимумов */
    for ( int i = 0; i < n; i++ ){
        if (x[i] > total_max)
            total_max = x[i];
    }
    
    return total_max;
}

int main(int argc, char* argv[]) {
    /* Внешние данные */
    int n = DEF_N;
    char * ENV_N = getenv("N");
    if (ENV_N)
        n = atoi(ENV_N);

    /* Объявление данных */
    double *x = new double[n];
    
    /* Подготовка данных */
    dataInitialization(x, n);
    
    /* Вычисление */
    auto time_start = Time::now();
    double result = findMaxElement(x, n);
    auto time_end = Time::now();
    fsec fs = time_end - time_start;

    /* Вывод результата */
    // printf("Total max = %10.2f\n", result);
    // printf("Time = %18.6f\n", fs.count());
    printf("%0.6f\n", fs.count());

    return 0;
}
