#include <mpi.h>
#include <cstdlib>
#include <cfloat>
#include <iostream>

using namespace std;

#define DEF_N 100

void dataInitialization(double *x, int n){
    for (int i=0; i < n; ++i)
        x[i] = i;
}

double findMaxElement(double *x, int n, int proc_rank, int proc_num){
    double total_max = -DBL_MAX, proc_max = -DBL_MAX;
    int proc_n = n / proc_num;

    /* РЕШЕНИЕ 1: распределение данных
    Размер вектора должен быть не меньше кол-ва процессов.
    Размер вектора должен нацело делиться на кол-во процессов.
    */
    // double *proc_x = new double[proc_n];
    // MPI_Scatter(x, proc_n, MPI_DOUBLE, proc_x, proc_n, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    /* Вычисление локальных максимумов */
    // for ( int i = 0; i < proc_n; i++ ){
    //     if (proc_x[i] > proc_max)
    //         proc_max = proc_x[i];
    // }

    /* РЕШЕНИЕ 1: широковещательная рассылка */
    MPI_Bcast(x, n, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    /* Вычисление локальных максимумов */
    int i1 = proc_n * proc_rank;
    int i2 = proc_n * ( proc_rank + 1 );
    if ( proc_rank == proc_num - 1 ) i2 = n;
    for ( int i = i1; i < i2; i++ ){
        if (x[i] > proc_max)
            proc_max = x[i];
    }

    /* Редукция максимумов */
    MPI_Reduce(&proc_max, &total_max, 1, MPI_DOUBLE, MPI_MAX, 0, MPI_COMM_WORLD);
    
    return total_max;
}

int main(int argc, char* argv[]) {
    /* Внешние данные */
    int n = DEF_N;
    char * ENV_N = getenv("N");
    if (ENV_N)
        n = atoi(ENV_N);

    /* Объявление данных */
    int proc_num, proc_rank;
    double *x = new double[n];

    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &proc_num);
    MPI_Comm_rank(MPI_COMM_WORLD, &proc_rank);
    
    /* Подготовка данных */
    if ( proc_rank == 0 ) dataInitialization(x, n);
    
    /* Вычисление */
    double time_start = MPI_Wtime();
    double result = findMaxElement(x, n, proc_rank, proc_num);
    double time_end = MPI_Wtime();

    /* Вывод результата */
    if ( proc_rank == 0 ){
        // printf("Total max = %10.2f\n", result);
        // printf("Time = %18.6f\n", time_end - time_start);
        printf("%0.6f\n", time_end - time_start);
    }

    MPI_Finalize();

    return 0;
}
