# MPI-Tasks

Сборка на кластере:
1. Подключить модули:
    ```
    module load gcc/9
    module load openmpi
    ```
2. Вызов компилятора: `mpic++ hello.cpp -o hello.exe`


Запуск:
- Простой: `mpirun ./hello.exe`
- Через планировщик:
    1.  Составление job.sh файла:
        ```bash
        #!/bin/sh
        module load openmpi
        mpirun ./hello.exe
        ```
        Далее: `chmod +x job.sh`
    2.  Отправление в очередь: `sbatch job.sh`
